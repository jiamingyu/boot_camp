# Types



?? types vs interface
https://blog.logrocket.com/types-vs-interfaces-typescript/

?? Return type
like : `ReturnType<typeof () => {...``}>`


?? `never` keyword

?? Template(string value) union type vs tagged union types





# Interfaces & classes

## interface is merged: same function can be defined multi times w. all functions merged as one

### Q: same function defined in > 1 interface, what is the outcome if inherits all

A: TS does NOT support interface function default implementation. No such issue exists

### 


# Control flow

## type check for single type
eg: `typeof v1 === "string"`.   // strict equal

## Intersection type narrow down like: `v1 instance of string`

##  `is` keyword
Eg:
`function isErrorResponse(obj: Response): obj is APIErrorResponse ` vs `function isErrorResponse(obj: Response): assert  obj is APIErrorResponse `

## spread syntax, rest parameters

Same as javascript :

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters

## as const
Subfields in objects are treated as though they can be mutated. eg 
`const args = [8, 5]` still can do put, or `args[0] = 22`

But `const args = [8, 5] as const` leave all above compile time exception

# class

## parameter def through constructor // Not exist in javascript

eg: ```constructor(public x: number, public y: number) {}```

## Decorator is just annotation

## `private x` vs `#private`

`#private` is runtime private and has enforcement inside the JavaScript engine that it is only accessible inside the class:

## `this` keyword is tricky

??? 
Thevalueof‘this’insideafunction depends on how the function is called. It is not guaranteed to always be the class instance. Youcanuse‘thisparameters’,use thebindfunction,orarrow functions to work around the issue when it occurs.

## 






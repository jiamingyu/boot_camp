# React design pattern discuss
https://www.patterns.dev/react/hooks-pattern/

## Hook pattern


### useReducer
https://react.dev/reference/react/useReducer

Below is helpful understanding how dispatch works with useReducer_called_func ( reducer() )

Ref: https://dev.to/dustinmyers/what-even-is-a-dispatch-function-27ma

```
function dispatch(action) {
  // check that the action argument is an object
  if (typeof action !== 'object' || obj === null) {
    throw new Error('actions must be plain object.');
  }

  // check that the action object has a 'type' property
  if (typeof action.type === 'undefined') {
    throw new Error('Actions may not have an undefined "type" property.');
  }

  // call the reducer and pass in currentState and action
  // capture the new state object in currentState, thus updating the state
  currentState = reducer(currentState, action);
}

```


# Master to follow

https://react-typescript-cheatsheet.netlify.app/docs/basic/setup

https://www.w3schools.com/react/react_hooks.asp

## DOM

https://legacy.reactjs.org/docs/react-component.html


https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction

DocumentObjectModel: The document as a whole, the head, tables within the document, table headers, text within the table cells, and all other elements in a document are parts of the document object model for that document. They can all be accessed and manipulated using the DOM and a scripting language like JavaScript.

## State

The state is a built-in React object that is used to contain data or information about the component. A component's state can change over time; whenever it changes, the component re-renders.

### State vs  Props

![Screenshot](prop_vss_state.png)

### Element vs Component vs DOM

### 

Presentational components are usually stateless: they do not contain their own React state

https://legacy.reactjs.org/docs/rendering-elements.html#:~:text=Elements%20are%20the%20smallest%20building,to%20match%20the%20React%20elements.


Elements are the smallest building blocks of React apps

Mutiple of elements composite to Component

React DOM takes care of updating the DOM to match the React elements.

Unlike browser DOM elements, React elements are plain objects, and are cheap to create. 

# Explore more on this site:

https://www.simplilearn.com/tutorials/reactjs-tutorial/reactjs-interview-questions

### What about DOM in React

https://www.freecodecamp.org/news/react-component-lifecycle-methods/#:~:text=The%20Mounting%20Phase%20begins%20when,is%20removed%20from%20the%20DOM.

React components have a lifecycle consisting of three phases: 

Mounting (then rendering), Updating(then rerendering), and Unmounting

## vscode run react typescript project

https://codedamn.com/news/reactjs/how-to-create-react-app-with-typescript

```
 npx create-react-app react-ts-demo --template typescript

 npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!




```
### Open for research

How to debug


## Following video demo below for next study

https://builtin.com/software-engineering-perspectives/create-react-app-typescript


## GraphQL 

Quick review: It is like protobuf(G-RPC), a communication interface more used by client/server (vs server/server by protobuf)

## Destructuring

https://basarat.gitbook.io/typescript/future-javascript/destructuring

https://codersera.com/blog/react-hooks-with-typescript-use-state-and-use-effect-in-2020/

https://dev.to/ayush_k_mandal/useeffect-hook-in-reactjs-with-typescript-beginner-5ch



Curly brackets ‘{ }’ are used to destructure the JavaScript Object propertie

#### Side effects

Are any actions that interact with the outside world, such as fetching data, updating the DOM, or using timers


### 


## api call example

```
const GetPost = async () => {
      const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'GET',
        headers: {
          "Content-Type": "application/json"
        }
      })
      const json = await response.json() as IDataModel[]
      if (response.ok)  { ... }
```

## React Functional component vs class component

https://www.geeksforgeeks.org/differences-between-functional-components-and-class-components/

The comparison table covers everything.

Notice: G4G talks about React in javascript instead of Typescript

Hooks can only be used in functional components, not in-class components.

## Hook



https://www.patterns.dev/react/hooks-pattern/


useXXX (I see that as DSL ), discussed headers

https://fettblog.eu/typescript-react/hooks/#useref

### CreateRef (class component) vs ForwardRef (functional component)

The typical pattern for modern React apps is to use only functional components (no class components). Since createRef is meant to be used with class components, stick to useRef and forwardRef for functional components.


### useEffect

React components have a lifecycle consisting of three phases: Mounting, Updating, and Unmounting

Lifecyclewise, `useEffect`` runs both after the first render and after every update; Then clean it up in componentWillUnmount()

# Decorator (aka Annotation) // I jump here because I read abount history of ts & care about the new features

https://www.typescriptlang.org/docs/handbook/decorators.html. //This doc is outdated, based on release notes below

https://devblogs.microsoft.com/typescript/announcing-typescript-5-0/#decorators


# TS style guides

https://google.github.io/styleguide/tsguide.html

https://docs.aws.amazon.com/prescriptive-guidance/latest/best-practices-cdk-typescript-iac/typescript-best-practices.html


## Legacy doc

https://legacy.reactjs.org/docs/introducing-jsx.html


